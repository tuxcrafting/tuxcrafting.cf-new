SRC = $(shell find src -regex '.*\.\(scm\|d\)')
BUILD = \
	$(SRC:src/%.scm=build/%.html) \
	$(SRC:src/%.d=build/%.cgi) \
	build/favicon.ico build/static/style.css build/static/pgp.txt build/robots.txt
CGI_D = $(shell echo ~/.dub/packages/cgi_d-*/cgi_d)

all: build build/blog build/cgi-bin $(BUILD)

build:
	mkdir -p $@
build/blog:
	mkdir -p $@
build/cgi-bin:
	mkdir -p $@

build/%: %
	mkdir -p $(shell dirname $@)
	cp $< $@

build/%.cgi: src/%.d
	ldc2 $< -of=$@ -I=$(CGI_D)/source -L=$(CGI_D)/bin/libcgi_d.a
	rm $(@:%.cgi=%.o)

build/blog/%.html: src/blog/%.scm blog-template.scm
	guile -s $< > $@

build/%.html: src/%.scm template.scm
	guile -s $< > $@

.PHONY: clean install deps

deps:
	dub fetch cgi_d
	dub build cgi_d

clean:
	rm -r build

install: $(BUILD)
	cp -r build/* /var/www/html
