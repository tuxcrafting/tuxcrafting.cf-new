(use-modules (sxml simple))

(define (sxml-template title body)
  `(html
    (head
     (title ,title)
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport") (content "width=device-width, initial-scale=1.0")))
     (meta (@ (name "description") (content "TuxCrafting's website")))
     (link (@ (rel "stylesheet") (href "/static/style.css")))
     (link (@ (rel "shortcut icon") (href "/favicon.ico")))
     (link (@ (rel "icon") (href "/favicon.ico"))))
    (body
     (h1 (@ (class "center")) "TuxCrafting's website")
     (ul (@ (class "center") (id "menu"))
	 (li (a (@ (href "/")) "Home"))
	 " "
	 (li (a (@ (href "/contact.html")) "Contact"))
         " "
         (li (a (@ (href "/cgi-bin/blog.cgi")) "Blog")))
     (hr)
     ,@body)))

(define (template title body)
  (sxml->xml (sxml-template title body)))
