(use-modules (sxml simple)
             (srfi srfi-19))

(define (blog title body)
  (sxml->xml `(,title
               "$$"
               ,(date->string
                 (current-date 0)
                 "~a ~Y-~m-~d ~H:~M:~S UTC")
               "$$"
               ,@body)))
