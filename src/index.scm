(load "../template.scm")

(template
 "Homepage"
 '((p
    "I'm just someone on the internet, who sometimes makes his computer crash, basically."
    (br)
    "My main interests are low-level things (networking, OSdev, ...). "
    "The main languages I use are C, D, and Python. "
    "I'm also trying to learn Scheme (Guile dialect).")
   (p "If you need my PGP key, it's " (a (@ (href "/static/pgp.txt")) "here") ".")
   (p
    "Most useful things I make are published to my " (a (@ (href "https://gitlab.com/tuxcrafting")) "Gitlab account") "."
    (br)
    "In fact, you can see the source code of this website " (a (@ (href "https://gitlab.com/tuxcrafting/tuxcrafting.cf-new")) "here") ".")))
