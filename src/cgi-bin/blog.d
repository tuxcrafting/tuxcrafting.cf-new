import cgi.cgi;

import std.file;
import std.stdio;
import std.string;

void main() {
	MIMETYPE type = MIMETYPE.TEXT_HTML_RAW;
	CGI cgi = new CGI(type);
	string template_path, title, date, pbody;
	try {
		if (cgi.getVal("post").length) {
			try {
				template_path = "../_blog-template.html";
				string[] post_text = readText("../blog/" ~ cgi.getVal("post") ~ ".html").split("$$");
				title = post_text[0];
				date = post_text[1];
				pbody = post_text[2];
			} catch (Exception) {
				return; // idk how to return a 404 with cgi_d, so i'm just returning nothing to make the browser angry
				// not a nice error message, sure, but it works
			}
		} else {
			template_path = "../_template.html";
			title = "Blog post list";
			pbody = "<ul>";
			foreach (string name; dirEntries("../blog", SpanMode.shallow)) {
				string[] post_text = readText(name).split("$$");
				string post_name = name.split("/")[2].split(".")[0];
				pbody ~= "<li><a href=\"/cgi-bin/blog.cgi?post=" ~ post_name ~ "\">" ~ post_text[0] ~ "</a> - " ~ post_text[1] ~ "</li>";
			}
			pbody ~= "</ul>";
		}
		cgi.pageStart();
		write(readText(template_path).replace("%TITLE%", title).replace("%BODY%", pbody).replace("%DATE%", date));
		cgi.pageEnd();
	} catch (Exception ex) {
		cgi.pageStart();
		writeln("<pre>" ~ ex.toString ~ "</pre>");
		cgi.pageEnd();
	}
}
