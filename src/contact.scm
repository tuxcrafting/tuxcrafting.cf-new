(load "../template.scm")

(template
 "Contact"
 '((ul
    (li "Email: " (a (@ (href "mailto:elie@agriates.com")) "elie@agriates.com")
	" (my parents have access to this address, so I'd prefer it if the emails are encrypted with PGP)")
    (li "Fediverse: " (a (@ (rel "me") (href "https://pleroma.tuxcrafting.cf/users/1")) "@tuxcrafting@pleroma.tuxcrafting.cf"))
    (li "XMPP: tuxcrafting@tuxcrafting.cf"))))
